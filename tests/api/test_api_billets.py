# -*- coding: utf-8 -*-

"""Test API Billets Absences

Utilisation :
        pytest tests/api/test_api_billets.py
"""
import datetime
import requests

from tests.api.setup_test_api import GET, POST_JSON, api_headers

ETUDID = 1


def test_billets(api_headers):
    """
    Ajout, Liste, Suppression billets absences

    Routes :
        - /billets_absence/create
        - /billets_absence/etudiant/<int:etudid>[?format=xml|json]
        - /billets_absence/delete
    """
    billet_d = dict(
        etudid=ETUDID,
        abs_begin="2022-07-31",
        abs_end="2022-08-01",
        description="test 1",
    )
    billet_r = POST_JSON("/billets_absence/create", billet_d, headers=api_headers)
    assert billet_r["etudid"] == billet_d["etudid"]
    assert datetime.datetime.fromisoformat(billet_r["abs_begin"]).replace(
        tzinfo=None
    ) == datetime.datetime.fromisoformat(billet_d["abs_begin"])
    billets = GET("/billets_absence/etudiant/1", headers=api_headers)
    assert isinstance(billets, list)
    assert len(billets) == 1
    assert billets[0] == billet_r
    billet_d2 = dict(
        etudid=ETUDID,
        abs_begin="2022-08-01",
        abs_end="2022-08-03",
        description="test 2",
    )
    billet_r = POST_JSON("/billets_absence/create", billet_d2, headers=api_headers)
    billets = GET("/billets_absence/etudiant/1", headers=api_headers)
    assert len(billets) == 2
    # Suppression
    for billet in billets:
        reply = POST_JSON(
            f"/billets_absence/{billet['id']}/delete", headers=api_headers
        )
        assert reply["OK"] == True
