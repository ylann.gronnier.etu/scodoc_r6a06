##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Vues sur les formations BUT

Emmanuel Viennet, 2023
"""

from flask import flash, g, redirect, render_template, request, url_for

from app import db
from app.but import cursus_but, validations_view
from app.decorators import (
    scodoc,
    permission_required,
)

from app.forms.formation.ue_parcours_ects import UEParcoursECTSForm

from app.models import (
    ApcParcours,
    ApcReferentielCompetences,
    Formation,
    FormSemestre,
    Identite,
    UniteEns,
)
from app.scodoc.codes_cursus import UE_STANDARD
from app.scodoc.sco_permissions import Permission
from app.scodoc.sco_exceptions import ScoPermissionDenied, ScoValueError
from app.views import notes_bp as bp
from app.views import ScoData


@bp.route("/parcour_formation/<int:formation_id>/<int:parcour_id>")
@bp.route("/parcour_formation/<int:formation_id>")
@scodoc
@permission_required(Permission.ScoView)
def parcour_formation(formation_id: int, parcour_id: int = None) -> str:
    """visu HTML d'un parcours dans une formation,
    avec les compétences, niveaux et UEs associées."""
    formation: Formation = Formation.query.filter_by(
        id=formation_id, dept_id=g.scodoc_dept_id
    ).first_or_404()
    ref_comp: ApcReferentielCompetences = formation.referentiel_competence
    if ref_comp is None:
        return "pas de référentiel de compétences"
    if parcour_id is None:
        parcour = None
    else:
        parcour: ApcParcours = ref_comp.parcours.filter_by(id=parcour_id).first()
        if parcour is None:
            raise ScoValueError("parcours invalide ou hors référentiel de formation")

    competences_parcour, ects_parcours = (
        cursus_but.parcour_formation_competences(parcour, formation)
        if parcour
        else (None, 0.0)
    )

    return render_template(
        "but/parcour_formation.j2",
        ects_parcours=ects_parcours,
        formation=formation,
        parcour=parcour,
        competences_parcour=competences_parcour,
        sco=ScoData(),
        title=f"{formation.acronyme} - Niveaux et UEs",
    )


@bp.route(
    "/validation_rcues/<int:formsemestre_id>/<int:etudid>/edit",
    defaults={"edit": True},
    endpoint="validation_rcues_edit",
)
@bp.route("/validation_rcues/<int:formsemestre_id>/<int:etudid>")
@scodoc
@permission_required(Permission.ScoView)
def validation_rcues(
    formsemestre_id: int, etudid: int = None, edit: bool = False
) -> str:
    """Visualisation des résultats UEs et RCUEs d'un étudiant
    et saisie des validation de RCUE antérieures.
    """
    etud: Identite = Identite.query.get_or_404(etudid)
    formsemestre: FormSemestre = FormSemestre.query.get_or_404(formsemestre_id)
    if edit:  # check permission
        if not formsemestre.can_edit_jury():
            raise ScoPermissionDenied(
                dest_url=url_for(
                    "notes.formsemestre_status",
                    scodoc_dept=g.scodoc_dept,
                    formsemestre_id=formsemestre_id,
                )
            )
    return validations_view.validation_rcues(etud, formsemestre, edit)


@bp.route("/ue_parcours_ects/<int:ue_id>", methods=["GET", "POST"])
@scodoc
@permission_required(Permission.EditFormation)
def ue_parcours_ects(ue_id: int):
    """formulaire (div) pour associer des ECTS par parcours d'une UE"""
    ue: UniteEns = (
        UniteEns.query.filter_by(id=ue_id)
        .join(Formation)
        .filter_by(dept_id=g.scodoc_dept_id)
        .first_or_404()
    )
    if ue.type != UE_STANDARD:
        raise ScoValueError("Pas d'ECTS / Parcours pour ce type d'UE")
    ref_comp = ue.formation.referentiel_competence
    if ref_comp is None:
        raise ScoValueError("Pas référentiel de compétence pour cette UE !")
    form = UEParcoursECTSForm(ue)

    edit_url = url_for("notes.ue_edit", scodoc_dept=g.scodoc_dept, ue_id=ue.id)
    if request.method == "POST":
        if request.form.get("submit"):
            if form.validate():
                for parcour in ue.formation.referentiel_competence.parcours:
                    field = getattr(form, f"ects_parcour_{parcour.id}")
                    if field:
                        ue.set_ects(field.data, parcour=parcour)
                db.session.commit()
                flash("ECTS enregistrés")
                return redirect(edit_url)
        elif request.form.get("cancel"):
            return redirect(edit_url)

    return render_template(
        "formation/ue_assoc_parcours_ects.j2", form=form, sco=ScoData(), ue=ue
    )
