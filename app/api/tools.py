##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################
"""ScoDoc 9 API : outils
"""

from flask_login import current_user
from sqlalchemy import desc, or_

from app import models
from app.scodoc.sco_utils import json_error
from app.models import Departement, Identite, Admission
from app.scodoc.sco_permissions import Permission


def get_etud(etudid=None, nip=None, ine=None) -> models.Identite:
    """
    L'instance d'étudiant la plus récente en fonction de l'etudid,
    ou du code nip ou code ine.

    etudid : None ou un int etudid
    nip : None ou un int code_nip
    ine : None ou un int code_ine

    Return None si étudiant inexistant.
    """
    allowed_depts = current_user.get_depts_with_permission(Permission.ScoView)

    if etudid is not None:
        query: Identite = Identite.query.filter_by(id=etudid)
    elif nip is not None:
        query = Identite.query.filter_by(code_nip=nip)
    elif ine is not None:
        query = Identite.query.filter_by(code_ine=ine)
    else:
        return json_error(
            404,
            message="parametre manquant",
        )
    if None not in allowed_depts:
        # restreint aux départements autorisés:
        query = query.join(Departement).filter(
            or_(Departement.acronym == acronym for acronym in allowed_depts)
        )
    etud = query.join(Admission).order_by(desc(Admission.annee)).first()
    # dans de rares cas (bricolages manuels, bugs), l'étudiant n'a pas de données d'admission
    if etud is None:
        etud = query.first()
    return etud
