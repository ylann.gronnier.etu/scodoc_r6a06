/* Page accueil département */
var apo_editor = null;
var elt_annee_apo_editor = null;
var elt_sem_apo_editor = null;

$(document).ready(function () {
  var table_options = {
    paging: false,
    searching: false,
    info: false,
    /* "autoWidth" : false, */
    fixedHeader: {
      header: true,
      footer: true,
    },
    orderCellsTop: true, // cellules ligne 1 pour tri
    aaSorting: [], // Prevent initial sorting
  };
  $("table.semlist").DataTable(table_options);
  let table_editable = document.querySelector("table#semlist.apo_editable");
  if (table_editable) {
    let save_url = document.querySelector("table#semlist.apo_editable").dataset
      .apo_save_url;
    apo_editor = new ScoFieldEditor(".etapes_apo_str", save_url, false);

    save_url = document.querySelector("table#semlist.apo_editable").dataset
      .elt_annee_apo_save_url;
    elt_annee_apo_editor = new ScoFieldEditor(
      ".elt_annee_apo",
      save_url,
      false
    );

    save_url = document.querySelector("table#semlist.apo_editable").dataset
      .elt_sem_apo_save_url;
    elt_sem_apo_editor = new ScoFieldEditor(".elt_sem_apo", save_url, false);
  }
});
