# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaires configuration Exports Apogée (codes)
"""

from flask_wtf import FlaskForm
from wtforms import SubmitField, validators
from wtforms.fields.simple import StringField

from app.models import SHORT_STR_LEN

from app.scodoc import codes_cursus


def _build_code_field(code):
    return StringField(
        label=code,
        default=code,
        description=codes_cursus.CODES_EXPL[code],
        validators=[
            validators.regexp(
                r"^[A-Z0-9_]*$",
                message="Ne doit comporter que majuscules et des chiffres",
            ),
            validators.Length(
                max=SHORT_STR_LEN,
                message=f"L'acronyme ne doit pas dépasser {SHORT_STR_LEN} caractères",
            ),
            validators.DataRequired("code requis"),
        ],
    )


class CodesDecisionsForm(FlaskForm):
    "Formulaire code décisions Apogée"
    ABAN = _build_code_field("ABAN")
    ABL = _build_code_field("ABL")
    ADC = _build_code_field("ADC")
    ADJ = _build_code_field("ADJ")
    ADJR = _build_code_field("ADJR")
    ADM = _build_code_field("ADM")
    ADSUP = _build_code_field("ADSUP")
    AJ = _build_code_field("AJ")
    ATB = _build_code_field("ATB")
    ATJ = _build_code_field("ATJ")
    ATT = _build_code_field("ATT")
    CMP = _build_code_field("CMP")
    DEF = _build_code_field("DEF")
    DEM = _build_code_field("DEM")
    EXCLU = _build_code_field("EXCLU")
    NAR = _build_code_field("NAR")
    PASD = _build_code_field("PASD")
    PAS1NCI = _build_code_field("PAS1NCI")
    RAT = _build_code_field("RAT")
    RED = _build_code_field("RED")

    NOTES_FMT = StringField(
        label="Format notes exportées",
        description="""Format des notes. Par défaut
            <tt style="font-family: monotype;">%3.2f</tt> (deux chiffres après la virgule)""",
        validators=[
            validators.Length(
                max=SHORT_STR_LEN,
                message=f"Le format ne doit pas dépasser {SHORT_STR_LEN} caractères",
            ),
            validators.DataRequired("format requis"),
        ],
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler", render_kw={"formnovalidate": True})
