# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaire changement formation
"""

from flask_wtf import FlaskForm
from wtforms import RadioField, SubmitField

from app.models import Formation


class FormSemestreChangeFormationForm(FlaskForm):
    "Formulaire changement formation d'un formsemestre"
    # construit dynamiquement ci-dessous


def gen_formsemestre_change_formation_form(
    formations: list[Formation],
) -> FormSemestreChangeFormationForm:
    "Create our dynamical form"

    # see https://wtforms.readthedocs.io/en/2.3.x/specific_problems/#dynamic-form-composition
    class F(FormSemestreChangeFormationForm):
        pass

    setattr(
        F,
        "radio_but",
        RadioField(
            "Label",
            choices=[
                (formation.id, formation.get_titre_version())
                for formation in formations
            ],
        ),
    )
    setattr(F, "submit", SubmitField("Changer la formation"))
    setattr(F, "cancel", SubmitField("Annuler"))
    return F()
